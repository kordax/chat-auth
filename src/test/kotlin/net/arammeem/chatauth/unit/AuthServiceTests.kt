package net.arammeem.chatauth.unit

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import net.arammeem.chatauth.crypto.CryptoService
import net.arammeem.chatauth.entity.AuthEntity
import net.arammeem.chatauth.entity.ServiceUserEntity
import net.arammeem.chatauth.repository.BackendUserRepository
import net.arammeem.chatauth.repository.ServiceUserRepository
import net.arammeem.chatauth.service.AuthService
import net.arammeem.chatauth.service.JwtService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
class AuthServiceTests {
    @MockK
    private lateinit var cryptoService: CryptoService
    @MockK
    private lateinit var jwtService: JwtService
    @MockK
    private lateinit var backendUserRepository: BackendUserRepository
    @MockK
    private lateinit var serviceUserRepository: ServiceUserRepository

    /**
     * Authorize user with valid credentials.
     *
     * @case positive: user credentials are valid
     * @result we should receive authorization entity
     */
    @Test
    fun AuthorizeUser_When_ValidCredentialsProvided_Success() {
        val authService = AuthService(
                cryptoService,
                jwtService,
                backendUserRepository,
                serviceUserRepository
        )

        // Predefined parameters
        val providedName = "UserName"
        val providedEmail = "testuser@email.org"
        val providedPassword = "paSsw0rd"
        /// End of 'Predefined Parameters'

        // Resulting mock variables
        val hash = "hash"
        val userId = "hash"
        val jwtToken = "token"
        /// End of 'Resulting mock variables'

        every { cryptoService.toMD5(any()) } returns hash
        every {
            serviceUserRepository.findByEmailAndHash(providedEmail, hash)
        } returns Optional.of(ServiceUserEntity(userId, providedName, providedEmail, hash))
        every { jwtService.generateJwtToken(any(), any()) } returns jwtToken

        val result: AuthEntity? = authService.authorizeByCredentials(email = providedEmail, password = providedPassword)

        Assertions.assertNotNull(result)
        Assertions.assertEquals(result?.jwtToken, jwtToken)
        Assertions.assertEquals(result?.userId, userId)
    }

    /**
     * Authorize user with invalid credentials.
     *
     * @case positive: user credentials are invalid
     * @result we should see old value for first flag and new value for second flag
     */
    @Test
    fun AuthorizeUser_When_InvalidCredentialsProvided_FailureWithNullResult() {
        val authService = AuthService(
                cryptoService,
                jwtService,
                backendUserRepository,
                serviceUserRepository
        )

        // Predefined parameters
        val providedEmail = "testuser@email.org"
        val providedPassword = "paSsw0rd"
        /// End of 'Predefined Parameters'

        // Resulting mock variables
        val hash = "hash"
        /// End of 'Resulting mock variables'

        every { cryptoService.toMD5(any()) } returns hash
        every {
            serviceUserRepository.findByEmailAndHash(providedEmail, hash)
        } returns Optional.empty()

        val result: AuthEntity? = authService.authorizeByCredentials(email = providedEmail, password = providedPassword)

        Assertions.assertNull(result)
    }
}