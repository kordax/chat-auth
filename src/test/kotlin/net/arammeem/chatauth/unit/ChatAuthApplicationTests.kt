package net.arammeem.chatauth.unit

import org.junit.Test
import org.junit.platform.runner.JUnitPlatform
import org.junit.platform.suite.api.SelectClasses
import org.junit.platform.suite.api.SuiteDisplayName
import org.junit.runner.RunWith

@SuppressWarnings("JUnit5Platform")
@RunWith(JUnitPlatform::class)
@SuiteDisplayName("Backend unit tests suite")
@SelectClasses(AuthServiceTests::class)
class ChatAuthApplicationTests {

    @Test
    fun contextLoads() {
    }

}
