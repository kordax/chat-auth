package net.arammeem.chatauth.repository

import net.arammeem.chatauth.entity.ServiceUserEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

/**
 * [ServiceUserEntity] repository used for authorization.
 *
 */
@Repository
interface ServiceUserRepository : CrudRepository<ServiceUserEntity, String> {
    fun findByEmailAndHash(email: String, hash: String): Optional<ServiceUserEntity>

    fun findByEmail(email: String): Optional<ServiceUserEntity>
}