package net.arammeem.chatauth.repository

import net.arammeem.chatauth.entity.BackendUserEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

/**
 * [BackendUserEntity] repository used to store converted content for.
 *
 * userId varchar(255) not null
 * constraint users_pkey
 * primary key,
 * meta text,
 * name varchar(255),
 * avatar_url varchar(255)
 */
@Repository
interface BackendUserRepository : CrudRepository<BackendUserEntity, String>