package net.arammeem.chatauth

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ChatAuthApplication

fun main(args: Array<String>) {
    runApplication<ChatAuthApplication>(*args)
}
