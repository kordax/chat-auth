package net.arammeem.chatauth.entity

import javax.persistence.*

/**
 * User entity for chat-backend 'users' table.
 *
 * userId varchar(255) not null constraint users_pkey primary key,
 * meta text,
 * name varchar(255),
 * avatar_url varchar(255)
 */
@Entity
@Table(name = "users")
data class BackendUserEntity(@Id val id: String, val name: String, val avatar_url: String, val meta: String)