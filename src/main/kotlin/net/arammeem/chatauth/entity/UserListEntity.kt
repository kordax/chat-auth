package net.arammeem.chatauth.entity

/**
 * User list entity.
 *
 * @param userList list of [ServiceUserEntity] entries
 */
data class UserListEntity(val userList: List<ServiceUserEntity>)