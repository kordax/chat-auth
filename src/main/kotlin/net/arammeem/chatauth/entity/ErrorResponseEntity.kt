package net.arammeem.chatauth.entity;

/**
 * REST entity for error responses.
 *
 */
data class ErrorResponseEntity(val message: String, val exception: Exception? = null)