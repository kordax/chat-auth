package net.arammeem.chatauth.entity

/**
 * Authorization entity.
 *
 * @param userId user userId
 * @param name name
 * @param email email
 * @param jwtToken JWT token
 */
data class AuthEntity(val userId: String, val name: String, val email: String, val jwtToken: String)
