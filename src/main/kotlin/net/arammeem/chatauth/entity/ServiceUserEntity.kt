package net.arammeem.chatauth.entity

import javax.persistence.*

/**
 * User entity for auth service 'registered_users' table only.
 * This entity doesn't relate to chat-backend 'users' table.
 *
 */
@Entity
@Table(name = "authorized_users")
data class ServiceUserEntity(@Id val id: String, val name: String, val email: String, val hash: String)