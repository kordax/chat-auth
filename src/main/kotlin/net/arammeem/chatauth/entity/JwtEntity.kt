package net.arammeem.chatauth.entity

import com.google.gson.annotations.SerializedName

/**
 * Jwt entry case class.
 *
 * @param jwtHeaderEntry jwt header
 * @param jwtPayloadEntry jwt payload
 * @param secret RS256 secret key
 */
data class JwtEntity(val jwtHeaderEntry: JwtHeaderEntry, val jwtPayloadEntry: JwtPayloadEntry, val secret: String)

/**
 * Jwt header entry case class.
 *
 * @param alg alg value
 * @param typ typ value
 */
data class JwtHeaderEntry(
        @SerializedName("alg") val alg: String,
        @SerializedName("typ") val typ: String
)

/**
 * Jwt payload entry case class.
 *
 * @param name user userId UUID value
 * @param email email
 * @param userId user UUID
 */
data class JwtPayloadEntry(
        @SerializedName("name") val name: String,
        @SerializedName("email") val email: String,
        @SerializedName("userId") val userId: String
)