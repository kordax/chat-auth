package net.arammeem.chatauth.service

import com.google.gson.Gson
import net.arammeem.chatauth.crypto.CryptoService
import net.arammeem.chatauth.entity.JwtEntity
import net.arammeem.chatauth.entity.JwtHeaderEntry
import net.arammeem.chatauth.entity.JwtPayloadEntry
import org.apache.tomcat.util.codec.binary.Base64
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.io.BufferedInputStream
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.security.GeneralSecurityException
import java.security.PrivateKey
import java.security.PublicKey

/**
 * JWT service.
 *
 */
@Service
class JwtService(private val cryptoService: CryptoService) {
    private val logger = LoggerFactory.getLogger(javaClass)
    private val gson = Gson()

    private final var privateKey: PrivateKey? = null
    private final var publicKey: PublicKey? = null

    /**
     * Initialize jwt object and read public/private key files.
     *
     * @throws Exception if error occurred
     */
    init {
        val pvtBis = BufferedInputStream(FileInputStream("jwt/private_key.der"))
        val pvtBytes = pvtBis.readAllBytes()
        val pubBis = BufferedInputStream(FileInputStream("jwt/public_key.der"))
        val pubBytes = pubBis.readAllBytes()

        privateKey = cryptoService.readPrivateKey(pvtBytes)
        publicKey = cryptoService.readPublicKey(pubBytes)
    }

    /**
     * Parse encoded Jwt String (including dot delimeter character) and return resulting entry as [JwtEntity].
     *
     * @param jwt jwt-token
     * @throws IllegalArgumentException if provided jwt is illegal
     * @throws FileNotFoundException if public key file wasn't found
     * @throws GeneralSecurityException on decryption error
     * @return jwt entry
     */
    fun parseJwt(jwt: String?): JwtEntity {
        if (jwt == null || jwt.isEmpty()) throw IllegalArgumentException(jwt)

        if (publicKey == null) throw IllegalStateException("Failed to process public key file")

        // Substring 'Bearer: '
        // eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJhMDAwMDAwMC0wMDAwLTQwMDAtYTAwMC0wMDAwMDAwMDAwMDAifQ.LHZvgD_7BqsyyD1w5oYkC9FqYdXcCJUjJkZScBAkz80UViAPeAzv_FaMDM83ZOL4-yZ7H721vWOJHk_zCHudNIXGOSY_3asril9QNSgbVlNaBYUzavEseJQnsNDZsJ8Zi4M9vw595tFdRntnFx-rq4LxrjzUE65iPYidxQnEN5u9cj33ho8Vz2yerMD1ggjd4YSy7dxGQpSZTvTPPba2zS25zPccvgGX2dopjJPCgFq2i-a8kS_jon2fEn2Xmj-bMF2Okmt5uPO_K351UL9lIxrdQC5hB7sLcT5c5raE4i1VcXrtQzRd-u-XEz9sVzmeTipnODAOIIlDi4PrYfkAQw

        val header: String
        val payload: String
        val signature: String

        try {
            header = jwt.substring(0, jwt.indexOf('.'))
            payload = jwt.substring(jwt.indexOf('.') + 1, jwt.lastIndexOf('.'))
            signature = jwt.substring(jwt.lastIndexOf('.') + 1)
        } catch(e: IndexOutOfBoundsException) {
            logger.error("Failed to parse jwt token: $jwt")
            throw e
        }

        if (header.isEmpty()) {
            throw IllegalArgumentException("Jwt header is not provided")
        }

        if (payload.isEmpty()) {
            throw IllegalArgumentException("Jwt payload is not provided")
        }

        if (signature.isEmpty()) {
            throw IllegalArgumentException("Jwt signature is not provided")
        }

        val base64 = Base64(true)

        val headerEntry = gson.fromJson(String(base64.decode(header)), JwtHeaderEntry::class.java)

        if (headerEntry.alg != "RS256") {
            throw IllegalArgumentException("Unsupported alg provided: ${headerEntry.alg}")
        }

        logger.debug("Signature string: $signature")
        val signatureDec = base64.decode(signature)

        logger.debug("Encrypted signature: $signatureDec")

        try {
            val sigBytes = cryptoService.decrypt(signatureDec, publicKey)
            return JwtEntity(headerEntry, gson.fromJson(String(base64.decode(payload)), JwtPayloadEntry::class.java), String(sigBytes))
        } catch (e: GeneralSecurityException) {
            throw e
        }
    }

    /**
     * Generate encoded jwt token.
     *
     * @param jwtHeaderEntry jwt header
     * @param jwtPayloadEntry jwt payload
     */
    fun generateJwtToken(jwtHeaderEntry: JwtHeaderEntry, jwtPayloadEntry: JwtPayloadEntry): String {
        if (privateKey == null) throw FileNotFoundException("Failed to open private key content")

        val headerStr = gson.toJson(jwtHeaderEntry)
        val payloadStr = gson.toJson(jwtPayloadEntry)

        val encHeaderStr = encodeJwtToBase64(headerStr.toByteArray())
        val encPayloadStr = encodeJwtToBase64(payloadStr.toByteArray())

        val signatureStr = "$encHeaderStr.$encPayloadStr"
        logger.debug("Signature size: ${signatureStr.toByteArray().size}")

        val signatureRSA = cryptoService.encrypt(signatureStr.toByteArray(), privateKey!!)
        logger.debug("Encrypted signature: $signatureRSA")
        logger.debug("Encrypted signature size: ${signatureRSA.size}")

        val encSignatureStr = encodeJwtToBase64(signatureRSA)
        return "$encHeaderStr.$encPayloadStr.$encSignatureStr"
    }

    /**
     * Encode Jwt bytes to Base64Url.
     *
     * @param jwt jwt bytes
     * @return result string
     */
    private fun encodeJwtToBase64(jwt: ByteArray): String {
        return Base64(true).encodeAsString(jwt).replace("\r\n", "")
    }
}