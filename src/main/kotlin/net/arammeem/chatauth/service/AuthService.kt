package net.arammeem.chatauth.service

import net.arammeem.chatauth.crypto.CryptoService
import net.arammeem.chatauth.entity.*
import net.arammeem.chatauth.repository.BackendUserRepository
import net.arammeem.chatauth.repository.ServiceUserRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Authorization service.
 *
 */
@Service
class AuthService(private val cryptoService: CryptoService,
                  private val jwtService: JwtService,
                  private val backendUserRepository: BackendUserRepository,
                  private val serviceUserRepository: ServiceUserRepository) {
    /**
     * Authorize user with name, email and password.
     *
     * @param email user email
     * @param password user password
     * @return null if user is not authorized, JWT token otherwise
     */
    fun authorizeByCredentials(email: String, password: String): AuthEntity? {
        val hash = cryptoService.toMD5(password.toByteArray())
        val user = serviceUserRepository.findByEmailAndHash(email, hash)

        if (user.isPresent) {
            return AuthEntity(userId = user.get().id,
                    name = user.get().name,
                    email = user.get().email,
                    jwtToken = jwtService.generateJwtToken(JwtHeaderEntry("RS256", "JWT"), JwtPayloadEntry(user.get().name, email, user.get().id)))
        }

        return null
    }

    /**
     * Authorize JWT token.
     *
     * @param token JWT token
     * @return null if user is not authorized, JWT token otherwise
     */
    fun authorizeByToken(token: String): AuthEntity? {
        try {
            val jwtEntity = jwtService.parseJwt(token)

            return AuthEntity(userId = jwtEntity.jwtPayloadEntry.userId, name = jwtEntity.jwtPayloadEntry.name, email = jwtEntity.jwtPayloadEntry.email, jwtToken = token)
        } catch (e: Exception) {
            throw e
        }
    }

    /**
     * Register user with name, email and password hash.
     *
     * @param id user userId
     * @param name user name
     * @param email user email
     * @param password user password
     * @throws IllegalArgumentException on illegal argument
     */
    @Transactional
    fun register(id: String, name: String, email: String, password: String): Boolean {
        if (id.isEmpty() || name.isEmpty() || email.isEmpty() || password.isEmpty()) {
            throw IllegalArgumentException()
        }

        if (serviceUserRepository.findByEmail(email).isPresent) {
            return false
        }

        val hash = cryptoService.toMD5(password.toByteArray())

        //@userId: String, name: String, email: String, hash: String
        backendUserRepository.save(BackendUserEntity(id, name, "", ""))
        serviceUserRepository.save(ServiceUserEntity(id = id, name = name, email = email, hash = hash))

        return true
    }

    /**
     * Retrieve user list.
     *
     */
    fun retrieveUserList(): Iterable<ServiceUserEntity> {
        return serviceUserRepository.findAll()
    }
}