package net.arammeem.chatauth.controller

import net.arammeem.chatauth.entity.ErrorResponseEntity
import net.arammeem.chatauth.entity.UserListEntity
import net.arammeem.chatauth.service.AuthService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.sql.SQLException
import java.util.*

/**
 * Authorization REST controller.
 *
 */
@RestController
class AuthController(private val authService: AuthService) {
    private val logger = LoggerFactory.getLogger(javaClass)
    /**
     * Authorization request with query parameters.
     *
     * @param email user email
     * @param password user password
     */
    @RequestMapping("/auth", method = [RequestMethod.GET])
    fun authorization(
            @RequestParam(value = "email", defaultValue = "") email: String,
            @RequestParam(value = "password", defaultValue = "") password: String
    ): ResponseEntity<Any> {
        logger.info("Authorizing user with credentials: $email, $password}")
        val authEntity = authService.authorizeByCredentials(email, password) ?: return ResponseEntity(ErrorResponseEntity("Failed to authorize user: $email, $password"), HttpStatus.UNAUTHORIZED)

        return ResponseEntity(authEntity, HttpStatus.OK)
    }

    /**
     * Registration request with query parameters.
     *
     * @param name user name
     * @param email user email
     * @param password user password
     */
    @RequestMapping("/register", method = [RequestMethod.PUT])
    fun registration(
            @RequestParam(value = "name", defaultValue = "") name: String,
            @RequestParam(value = "email", defaultValue = "") email: String,
            @RequestParam(value = "password", defaultValue = "") password: String
    ): ResponseEntity<Any> {
        logger.info("Registering user with credentials: $name, $email, $password}")

        val id = UUID.randomUUID().toString()

        try {
            if (!authService.register(id, name, email, password)) {
                logger.error("Failed to register user, user with this email already exists: $email")

                return ResponseEntity(ErrorResponseEntity("Failed to register user, user with this email already exists: $email"), HttpStatus.CONFLICT)
            }
        } catch (e: SQLException) {
            logger.error("Failed to register user with credentials: $name, $email, $password", e)

            return ResponseEntity(ErrorResponseEntity("Failed to register user with credentials: $name, $email, $password", e), HttpStatus.INTERNAL_SERVER_ERROR)
        } catch (ie: IllegalArgumentException) {
            logger.error("Failed to register user with credentials, illegal arguments provided", ie)

            return ResponseEntity(ErrorResponseEntity("Failed to register user with credentials, illegal arguments provided", ie), HttpStatus.INTERNAL_SERVER_ERROR)
        }

        return ResponseEntity("{}", HttpStatus.OK)
    }

    /**
     * User list retrieval request with query parameters.
     *
     */
    @RequestMapping("/userlist", method = [RequestMethod.GET])
    fun userList(@RequestParam(value = "token", defaultValue = "") token: String): ResponseEntity<UserListEntity> {
        logger.info("Received request to retrieve user list")

        return try {
            val userList = authService.retrieveUserList()

            ResponseEntity(UserListEntity(userList.toList()), HttpStatus.OK)
        } catch (e: SQLException) {
            logger.error("Failed to retrieve user list", e)

            ResponseEntity(UserListEntity(listOf()), HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}