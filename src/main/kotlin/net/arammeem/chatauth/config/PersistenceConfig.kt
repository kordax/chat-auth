/*
 * [PersistenceConfig.java]
 * [chat-backend]
 *
 * Created by [Dmitry Morozov] on 03 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatauth.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.transaction.annotation.EnableTransactionManagement

import javax.sql.DataSource

/**
 * Persistence configuration.
 *
 */
@Configuration
@EnableJpaRepositories("net.arammeem.chatauth.repository")
@EnableTransactionManagement
class PersistenceConfig {
    @Value("\${backend.db.connection.url}")
    private val url: String? = null
    @Value("\${backend.db.connection.user}")
    private val user: String? = null
    @Value("\${backend.db.connection.password}")
    private val password: String? = null

    /**
     * Configured data source bean.
     *
     * @return configured [DataSource]
     */
    @Bean
    fun dataSource(): DataSource {
        val dataSource = DriverManagerDataSource()
        dataSource.setDriverClassName(org.postgresql.Driver::class.java.name)

        dataSource.url = url
        dataSource.username = user
        dataSource.password = password

        return dataSource
    }
}