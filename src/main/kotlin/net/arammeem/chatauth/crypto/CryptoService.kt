package net.arammeem.chatauth.crypto

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.security.KeyFactory
import java.security.MessageDigest
import java.security.PrivateKey
import java.security.PublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher
import javax.xml.bind.DatatypeConverter


/**
 * Cryptography service that provides crypto operations.
 *
 */
@Service
class CryptoService {
    private val logger = LoggerFactory.getLogger(javaClass)
    private val alg = "RSA"
    private val encCipher = Cipher.getInstance(alg)
    private val decCipher = Cipher.getInstance(alg)

    /**
     * Encrypt data using private key.
     *
     * @param data data
     * @param privateKey private key
     * @throws Exception if error occurred
     * @return encrypted data as String
     */
    fun encrypt(data: ByteArray, privateKey: PrivateKey?): ByteArray {
        logger.debug("Encrypting ${data.size} bytes")
        encCipher.init(Cipher.ENCRYPT_MODE, privateKey)
        return encCipher.doFinal(data)
    }

    /**
     * Decrypt data using public key.
     *
     * @param data data
     * @param publicKey public key
     * @throws Exception if error occurred
     * @return decrypted data as String
     */
    fun decrypt(data: ByteArray, publicKey: PublicKey?): ByteArray {
        logger.debug("Decrypting ${data.size} bytes")
        decCipher.init(Cipher.DECRYPT_MODE, publicKey)
        return decCipher.doFinal(data)
    }

    /**
     * Read public key from file source bytes.
     *
     * @param source source bytes
     * @param alg algorithm
     * @throws Exception if error occurred
     * @return
     */
    fun readPublicKey(source: ByteArray, alg: String = this.alg): PublicKey {
        val keySpec = X509EncodedKeySpec(source)
        val kf = KeyFactory.getInstance(alg)

        logger.debug("Reading public key")

        return kf.generatePublic(keySpec)
    }

    /**
     * Read private key from file source bytes.
     *
     * @param source source bytes
     * @param alg algorithm
     * @throws Exception if error occurred
     * @return
     */
    fun readPrivateKey(source: ByteArray, alg: String = this.alg): PrivateKey {
        val keySpec = PKCS8EncodedKeySpec(source)
        val kf = KeyFactory.getInstance(alg)

        logger.debug("Reading private key")

        return kf.generatePrivate(keySpec)
    }

    /**
     * Generate MD5 from byte array.
     *
     * @param bytes bytes
     * @return MD5 HEX string
     */
    fun toMD5(bytes: ByteArray): String {
        val md = MessageDigest.getInstance("MD5")
        md.update(bytes)
        val digest = md.digest()

        return DatatypeConverter.printHexBinary(digest)
    }
}