# Authorization server application readme

This application is built upon Spring Boot and Gradle.
Use gradle commands to build/run this application or use provided IntelliJ IDEA project.

> Please note that you have to configure your hosts file to include db-host and kafka-host entries.

## Requirements

1) Kotlin >1.2;
2) Gradle >4.10.*.

## How to build
1. `gradle build -x test` or `gradle build` to run tests.

## How to run application

You have several options to run this application:
1. Pass java jar manually: `java -jar build/libs/chat-backend-$VERSION.jar` with application arguments using `-Darg=1`;
    * Do the same for dev profile with debug on: `java -Dspring.profiles.active=dev -jar build/libs/chat-backend-0.3.65.1.jar`.
2. `gradle bootRun` (not recommended).

## Available configuration parameters
# Database configuration
| Parameter name                    | Default value                   | Description                         |
|-----------------------------------|---------------------------------|-------------------------------------|
| backend.db.connection.user        | postgres                        | PostgreSQL user name.               |
| backend.db.connection.password    | R3dMadRobot                     | PostgreSQL database password.       |
| backend.db.connection.url         | jdbc:postgresql://db-host:5432/ | PostgreSQL database connection url.*|
> Connection url format: jdbc:postgresql://$DB_LISTENER_HOST:$DB_LISTENER_PORT/$DB_NAME.
If $DB_NAME isn't provided user name will be used.

# How it works
There are two URI REST mappings available:
1. `$SERVER_HOST:$SERVER_PORT/auth`. This mapped controller expects `GET` request and returns JWT TOKEN.
    * Mandatory URL query parameters: name, email, password.
2. `$SERVER_HOST:$SERVER_PORT/register`. This mapped controller expects `PUT` request and it's main objective is to
 register new user.
    * Mandatory URL query parameters: name, email, password, token.
3. `$SERVER_HOST:$SERVER_PORT/userlist`. Retrieve user list.
    
    
## Operations examples with CURL
### Authorization
```curl -i -sS '10.110.10.185:8081/auth?email=d.morozov@redmadrobot.com&password=12345'
 HTTP/1.1 200
 Content-Type: application/json;charset=UTF-8
 Transfer-Encoding: chunked
 Date: Tue, 27 Nov 2018 12:45:05 GMT
 
 {"userId":"a0000000-0000-4000-a000-000000000000","jwtToken":someToken"}
 ```
### Registration
```
curl -i -X PUT -d id=a0000000-0000-4000-a000-000000000000 -d name="Dmitry Morozov" -d email=d.morozov@redmadrobot.com -d password=12345 10.110.10.185:8081/register
```

### User list
```
curl -i 10.110.10.185:8081/userlist
```

## Available query parameters
| Parameter name | Description     |
|----------------|-----------------|
| name           | User name       |
| email          | User email      |
| password       | User password   |